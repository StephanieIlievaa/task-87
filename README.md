# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

# A JavaScript Task
In this task we have to install **Ramda** library and by using **pluck** method on the provided article elements.

## Objective
* Checkout the dev branch.
* Install the Ramda package, import it and use the **pluck** method on the provided array to extract the 3 classes.
* Here you can learn more about Ramda - https://ramdajs.com/docs/
* All the articles are already selected with **querySelectorAll** and to add a class to each of the articles .


## Requirements
* The project starts with **npm run start**.
* Install the Ramda library with **npm i ramda**.
* When implemented merge dev branch into master.
## Gotchas
* Each article has an existing class and you have to add the new one without overriding the old class.